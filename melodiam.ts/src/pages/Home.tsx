import * as React from 'react';
import { Provider } from 'react-redux';
import 'ui/css/Home.css';
import { ScreenInfo } from 'ui';
import User from 'ui/User';
import CurrentSong from 'ui/CurrentSong';
import ListenAlong from 'ui/ListenAlong';
import PreviousSongs from 'ui/PreviousSongs';
import { APP_STORE } from 'store';

// tslint:disable-next-line: no-default-export
export default class Home extends React.PureComponent {
  render() {
    return (
      <div className="centered-container">
        <Provider store={APP_STORE} >
          <ScreenInfo />
          <User />
          <CurrentSong />
          <div className="action-buttons">
            <ListenAlong />
            <PreviousSongs />
          </div>
        </Provider>
      </div>
    );
  }
}
