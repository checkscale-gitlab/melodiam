import Button from '@material-ui/core/Button'
import HearingIcon from '@material-ui/icons/Hearing'
import * as React from 'react'
import 'ui/css/ListenAlong.css'

// tslint:disable-next-line: no-default-export
export default class ListenAlong extends React.PureComponent {
  render() {
    return (
      <Button disabled={true} onClick={this.redirectToListenAlong} variant="contained" color="primary" size="small" fullWidth={true}>
        <HearingIcon className="left-icon" />
        Listen Along
      </Button>
    )
  }

  private redirectToListenAlong = () => window.open("/api/listen_along", "_self");
}
