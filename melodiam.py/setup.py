#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import re

from setuptools import find_packages, setup


def get_version(package):
    """
    Return package version as listed in `__version__` in `__init__.py`.
    """
    with open(os.path.join(package, "__init__.py")) as f:
        return re.search("__version__ = ['\"]([^'\"]+)['\"]", f.read()).group(1)


def get_long_description():
    """
    Return the README.
    """
    with open("README.md", encoding="utf8") as f:
        return f.read()


setup(
    name="melodiam",
    python_requires=">=3.7",
    version=get_version("melodiam"),
    url="https://gitlab.com/harry.sky.vortex/melodiam",
    license="Unlicense",
    description="Monitor your Spotify listening and let people listen along.",
    long_description=get_long_description(),
    long_description_content_type="text/markdown",
    author="Igor Nehoroshev",
    author_email="mail@neigor.me",
    packages=["melodiam"],
    data_files=[("", ["LICENSE"])],
    include_package_data=True,
    install_requires=[
        "tekore>=1.1.*",
        "httpx==0.11.*",
        "apscheduler==3.6.*",
        "starlette>=0.12.11",
        "itsdangerous",
        "uvicorn",
    ],
    extras_require={"lint": ["mypy", "autoflake", "black", "isort"],},
    classifiers=[
        "Development Status :: 4 - Beta",
        "Environment :: Web Environment",
        "Intended Audience :: Developers",
        "License :: Public Domain",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
    ],
)
